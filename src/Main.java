import com.mdraco.chat.BaseClient;
import com.mdraco.chat.ConsoleClient;
import com.mdraco.chat.Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Observable;
import java.util.Observer;

public class Main {

	private static int port = 6666;

	public static void main(String[] args) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		if (args.length > 0 && args[0].equalsIgnoreCase("s")) {
			System.out.println("server mode started");
			try {
				Server s = new Server(port);

				String text = reader.readLine();
				while (!text.equalsIgnoreCase("q") && !text.equalsIgnoreCase("quit")) {
					text = reader.readLine();
				}
				s.Close();
			} catch (IOException e) {
				e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
			}
		}
		else if (args.length > 0 && args[0].equalsIgnoreCase("c")) {
			System.out.println("client mode started");
			try {
				BaseClient c = new ConsoleClient("localhost", port);

				String text = reader.readLine();
				while (!text.equalsIgnoreCase("q") && !text.equalsIgnoreCase("quit")) {
					c.Send(text);
					text = reader.readLine();
				}
				c.Close();
			} catch (IOException e) {
				e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
			}
		}
		else {
			System.out.println(args.length);
			if (args.length > 0)
				System.out.println(args[0]);
			System.out.println("start with 'c' (client mode) or 's' (server mode).");
		}
		System.out.println("bye");
    }
}
