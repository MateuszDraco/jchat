package com.mdraco.chat;

import java.io.*;
import java.net.Socket;

/**
 * Created with IntelliJ IDEA.
 * User: mateusz
 * Date: 24.03.2013
 * Time: 15:42
 */
public abstract class BaseClient {
	protected final Socket socket;
	//protected final OutputStreamWriter outWriter;
	protected final InputStreamReader outReader;
	protected final BufferedReader reader;
	//protected final BufferedWriter writer;
	private final PrintStream printer;

	public BaseClient(String server, int port) throws IOException
	{
		this(new Socket(server, port));
	}

	public BaseClient(Socket socket) throws IOException {
		this.socket = socket;
		//this.outWriter = new OutputStreamWriter(socket.getOutputStream());
		this.outReader = new InputStreamReader(socket.getInputStream());
		this.reader = new BufferedReader(outReader);
		//this.writer = new BufferedWriter(outWriter);
		this.printer = new PrintStream(socket.getOutputStream());

		startReading();
	}

	public void Send(String text)
	{
//		System.out.print("sending ok=");
//		System.out.println(!this.socket.isClosed());
		printer.println(text);
//		try {
//
//			this.writer.append(text);
//			this.writer.newLine();
//			//this.writer.flush();
//		} catch (IOException e) {
//			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//		}
	}

	public void Close()
	{
		try {
			this.printer.close();
			//this.writer.close();
			//this.outWriter.close();
			this.reader.close();
			this.outReader.close();

			this.socket.close();
		} catch (IOException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
	}

	private void startReading() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("start listening");
				try {
					String text = reader.readLine();
					while (text != null) {
						//System.out.print("found: ");
						//System.out.println(text);
						onIncomingMessage(text);
						text = reader.readLine();
					}
				} catch (IOException e) {
					//e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
					System.out.println("read failed.");
				}
				System.out.println("read finished.");
			}
		}).start();
	}

	public abstract void onIncomingMessage(String text);
}
