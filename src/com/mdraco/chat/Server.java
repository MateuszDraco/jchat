package com.mdraco.chat;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: mateusz
 * Date: 13.04.2013
 * Time: 09:07
 * To change this template use File | Settings | File Templates.
 */
public class Server {
	private final ServerSocket socket;
	private final ArrayList<BaseClient> clients;
	private Thread listeningThread;

	public Server(int port) throws IOException {
		this.socket = new ServerSocket(port);
		this.clients = new ArrayList<BaseClient>();

		this.startListening();
	}

	public void startListening()
	{
		this.listeningThread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {

					while (true) {
						Socket s = Server.this.socket.accept();

						addClient(new ServerClient(Server.this, s));
					}
				} catch (IOException e) {
					System.out.println("server closed.");
					//e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
				}
			}
		});
		this.listeningThread.start();
	}

	private synchronized void addClient(BaseClient client) {
		System.out.println("new client!");
		clients.add(client);
	}

	public void Close()
	{
		try {
			this.socket.close();
			this.listeningThread.join();
		} catch (IOException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		} catch (InterruptedException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
	}

	public synchronized void broadcastMessage(ServerClient client, String text) {
		for (BaseClient c : clients) {
			if (c != client) {
				c.Send(text);
			}
		}
	}
}
