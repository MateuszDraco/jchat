package com.mdraco.chat;

import java.io.IOException;
import java.net.Socket;

/**
 * Created with IntelliJ IDEA.
 * User: mateusz
 * Date: 13.04.2013
 * Time: 12:04
 * To change this template use File | Settings | File Templates.
 */
public class ServerClient extends BaseClient {
	private final Server server;

	public ServerClient(Server parent, Socket socket) throws IOException {
		super(socket);

		this.server = parent;
	}

	@Override
	public void onIncomingMessage(String text) {
		server.broadcastMessage(this, text);
	}
}
