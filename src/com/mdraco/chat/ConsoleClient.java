package com.mdraco.chat;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: mateusz
 * Date: 13.04.2013
 * Time: 12:08
 * To change this template use File | Settings | File Templates.
 */
public class ConsoleClient extends BaseClient {
	public ConsoleClient(String server, int port) throws IOException {
		super(server, port);
	}

	@Override
	public void onIncomingMessage(String text) {
		System.out.println(text);
	}
}
